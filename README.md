# Suggested Reviewer Client Examples

This repository contains examples of all the C-based gRPC implementations of
Suggested Reviewer client. Each language subdirectory contains the protocol
buffer in the corresponding binding and more:

- Ruby

## Prerequisites

A running instance of the [Suggested Reviewer Service](https://gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/recommender-bot-service).

## Ruby

1. Install dependencies

   ```shell
   bundle install
   ```

1. Update the address of the Suggested Reviewer server in the clients.
1. Update the request to use the relevant data on the local environment. For
   example, project details.
1. Run the client

   ```shell
   # Secure gRPC client (with TLS certificate)
   ./ruby/secure_grpc_client

   # Insecure gRPC client (without TLS certificate)
   ./ruby/insecure_grpc_client
   ```
